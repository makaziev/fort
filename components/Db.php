<?php



class Db
{

	public static function getConnect()
	{
		$paramsPath = ROOT . '/config/db_params.php';
		$params = include($paramsPath);

		$db = new PDO("mysql:dbname={$params['dbname']};host={$params['host']}", $params['user'], $params['password']);

		return $db;
	}

}