<?php

return [

	'news/([0-9]+)' => 'news/view/$1',
	'news/delete/([0-9]+)' => 'news/delete/$1',
	'news/add' => 'news/add',
	'news/search' => 'news/search',
	'news/make-data' => 'news/makedata',
	'news' => 'news/index',
	'' => 'news/index',
];