<?php


include_once ROOT . '/models/News.php';


class NewsController
{

	public function actionIndex()
	{

		$news = News::getNews();

		$content = ROOT . '/views/news/index.php';

		require_once ROOT . '/views/layouts/main.php';

		return true;

	}

	public function actionView($id)
	{
		$newsItem = News::getNewsById($id);

		$content = ROOT . '/views/news/view.php';

		require_once ROOT . '/views/layouts/main.php';

		return true;
	}

	public function actionAdd()
	{

		if (empty($_POST)) {

			$content = ROOT . '/views/news/form.php';

			require_once ROOT . '/views/layouts/main.php';

			return true;
		}
		else {

			News::addNews();
			header('Location: /news');
		}

	}

	public function actionDelete($id)
	{
		News::deleteNews($id);
		header('Location: /news');
	}

	public function actionSearch()
	{

		if (!empty($_POST['search'])) {

			$news = News::getSearch($_POST['search']);

			$content = ROOT . '/views/news/search.php';

			require_once ROOT . '/views/layouts/main.php';

			return true;
		}

		return header('Location: /news');

	}

	public function actionMakedata()
	{
		News::makeData();
		return header('Location: /news');
	}

}