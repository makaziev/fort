<?php



class News
{

	public static function getNewsById($id)
	{
		$id = intval($id);

		if ($id) {

			$db = Db::getConnect();

			$result = $db->query('SELECT * FROM news WHERE id =' . $id);
			$result->setFetchMode(PDO::FETCH_ASSOC);

			$newsItem = $result->fetch();

			return $newsItem;
		}

	}

	public static function getNews()
	{

		$db = Db::getConnect();

		$result = $db->query('SELECT * FROM news ORDER BY id DESC');

		if ($result) {

			$result->setFetchMode(PDO::FETCH_ASSOC);

			$newsItem = $result->fetchAll();

			return $newsItem;
		}

	}

	public static function addNews()
	{

		$title = $_POST['title'];
		$text = $_POST['text'];

		$db = Db::getConnect();

		$sql = "INSERT INTO `news` (`title`, `text`) VALUES (?, ?);";

		$stmt = $db->prepare($sql);

		$stmt->execute([$title, $text]);
	}

	public static function deleteNews($id)
	{

		$db = Db::getConnect();

		$sql = "DELETE FROM news WHERE id =" . $id;
		$stmt = $db->prepare($sql); 
		$stmt->execute();

	}

	public static function getSearch($search)
	{

		$db = Db::getConnect();

		$result = $db->query("SELECT * FROM news WHERE title LIKE '%$search%' OR `text` LIKE '%$search%'");
		$result->setFetchMode(PDO::FETCH_ASSOC);

		$news = $result->fetchAll();

		return $news;

	}

	public static function makeData()
	{

		$db = Db::getConnect();

		$sql = "CREATE TABLE `news` (
			 `id` int(11) NOT NULL AUTO_INCREMENT,
			 `title` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
			 `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
			 PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
			";

		$sql2 = "INSERT INTO `news` (`id`, `title`, `text`) VALUES (NULL, 'Haval представил новый внедорожник со странным названием', 'На автосалоне в Чэнду дебютировала новая модель Haval с нетипичным для бренда дизайном и названием: DaGou, что дословно переводится с китайского как «большая собака». Облик новинки, которая сравнима по габаритам с Nissan X-Trail, разработал бывший дизайнер Jaguar Land Rover Фил Симмонс.'), (NULL, 'Мотоциклистов хотят вывести из «сумрака»', 'Жизнь мотоциклистов может серьезно усложниться депутаты Госдумы предлагают наказывать их за опасное лавирование и движение между рядами машин. Штрафы предусмотрены от 3 до 15 тыс. руб. Кроме того, мотоциклиста могут лишить прав на срок от двух до шести месяцев. Авторы законопроекта полагают, что такие меры защитят всех участников движения. Обычная ширина дорожных полос в городах чуть меньше 3 м не позволяет вместить и четырех- и двухколесный транспорт.'), (NULL, 'История поезда, который готов соперничать с самолетом', 'Сто лет назад летающий поезд казался такой же сказочной небылицей, как ковер-самолет или полет на Луну из пушки, но в наше время поезда, летящие над дорожным полотном, уже стали реальностью. Уже в начале ХХ были достаточно хорошо изучены физические принципы магнитной левитации, а в 1934 году немецкий инженер Герман Кемпер запатентовал поезд на магнитной подушке, который он назвал «магнитоплан». Именно он и стал прообразом будущих маглевов.'), (NULL, 'Ситимобил будет доставлять технику из интернет-магазина', 'Компания «Связной» совместно с сервисом заказа такси Ситимобил запустили экспресс-доставку товаров из интернет-магазина svyaznoy.ru. Такая услуга сейчас доступна жителям 12 районов Москвы, в ближайшее время будет подключен весь город.'), (NULL, 'В Mercedes создали фронтальные подушки для задних пассажиров', 'В немецкой компании рассказали, что Mercedes-Benz S-Класса станет первым автомобилем в мире, оснащенным фронтальными подушками безопасности для пассажиров второго ряда, которые предназначены для защиты при лобовом столкновении.');";

		$stmt = $db->prepare($sql);
		$stmt->execute();

		$stmt = $db->prepare($sql2);
		$stmt->execute();
	}

}