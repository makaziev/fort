
<div class="container">
  <div class="row">
    <div class="col-8">
    	<p>
			<a class="btn btn-primary" href="/news" role="button">Назад</a>
		</p>
    	<div class="list-group">
    		<form method="post" action="/news/search">
			  <div class="form-row align-items-center">
			    <div class="col-auto" style="width: 88%">
			      <input type="text" name="search" class="form-control mb-2" placeholder="Поиск...">
			    </div>
			    <div class="col-auto">
			      <button type="submit" class="btn btn-primary mb-2">Поиск</button>
			    </div>
			  </div>
			</form>
			<br>
		  <?php foreach ($news as $value) : ?>
		  	<a href="/news/<?php echo $value['id'] ?>" class="list-group-item list-group-item-action">
			    <div class="d-flex w-100 justify-content-between">
			      <h5 class="mb-1"><?php echo $value['title'] ?></h5>
			    </div>
			    <p class="mb-1"><?php echo $value['text'] ?></p>
			</a>
		  <?php endforeach; ?>
		</div>
    </div>
    <div class="col-4"></div>
  </div>
</div>