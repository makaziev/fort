
<div class="container">
  <div class="row">
    <div class="col-8">
    	<p>
			<a class="btn btn-primary" href="/news" role="button">Назад</a>
			<a class="btn btn-danger" href="/news/delete/<?php echo $newsItem['id'] ?>" role="button">Удалить</a>
		</p>
    	<div class="list-group">
			<a href="#" class="list-group-item list-group-item-action">
				<div class="d-flex w-100 justify-content-between">
			  		<h5 class="mb-1"><?php echo $newsItem['title'] ?></h5>
				</div>
				<p class="mb-1"><?php echo $newsItem['text'] ?></p>
			</a>
		</div>
    </div>
    <div class="col-4"></div>
  </div>
</div>